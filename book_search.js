/**
 * Searches for matches in scanned text.
 * @param {string} searchTerm - The word or term we're searching for. 
 * @param {JSON} scannedTextObj - A JSON object representing the scanned text.
 * @returns {JSON} - Search results.
 * */ 
 function findSearchTermInBooks(searchTerm, scannedTextObj) {
    var result = {
        "SearchTerm": searchTerm,
        "Results": []
    };

    /** Verify types of arguments. It also works as a null check. */
    if (typeof(searchTerm) != "string" || !Array.isArray(scannedTextObj))
        return result;

    /** Remove whitespace from start and end of the search term. */
    const searchTermTrimmed = searchTerm.trim();
    if (searchTermTrimmed.length <= 0)
        return result;

    /* RegExp to match search term.
     * Add word boundaries (\b) to do a "whole words only" search. 
     * (?!-$) checks if a hyphen at the end of the line DOES NOT exist, then validates
     * the pattern before it, which in this case is: \b<searchTerm>\b
     * This prevents a search term matching a hyphenated line break (Test 6 & 7).
     * */
    const searchRegex = new RegExp('\\b' + escapeRegex(searchTermTrimmed) + '\\b(?!-$)');

    /* RegExp to match word before the hyphen at the end of the line.
    * (?=-$) checks if a hyphen at the end of the line DOES exist, then validates
    * the pattern before it, which in this case is: [\w'-]+
    * [\w'-]+ allow words (\w), apostrophes (') and hyphens (-) to prefix the hyphen
    * in case compound words are split with a hyphenated line break (Test 4 & 5).
    * */
    const hyphenatedEOLPattern = /[\w'-]+(?=-$)/;

    /** Get matching lines */
    for (const bookObj of scannedTextObj) {
        /** Verify structure of the scanned book */
        if (bookObj == null || typeof(bookObj.ISBN) != "string" || !Array.isArray(bookObj.Content))
            continue;

        for (let i = 0; i < bookObj.Content.length; i++) {
            /** Verify structure of the scanned line */
            const lineObj = bookObj.Content[i];
            if (!isLineObjValid(lineObj))
                continue;

            var match = {
                "ISBN": bookObj.ISBN,
                "Page": lineObj.Page,
                "Line": lineObj.Line
            };

            /** Remove spaces from start and end of line. Just in case of a rare case where there is a space after a hyphenated line break. */
            const lineTrim = lineObj.Text.trim();

            /** Add the line to results if it contains the search term. */
            if (searchRegex.test(lineTrim)) {
                result.Results.push(match);
            }

            /* Match hyphenated words with next line.
            * 
            * Check if the word before the hyphenated line break is the
            * same as the first part of our search term.
            * */
            if (hyphenatedEOLPattern.test(lineTrim) && isLineObjValid(bookObj.Content[i + 1])) {
                const hyphenatedEOLMatch = lineTrim.match(hyphenatedEOLPattern)[0];
                const firstPart = searchTermTrimmed.substring(0, hyphenatedEOLMatch.length);
                if (firstPart == hyphenatedEOLMatch) {
                    /** This assumes lines are in order in the array and that there is only one object per line. */
                    const nextLineObj = bookObj.Content[i + 1];
                    const nextLineObjTextTrimmed = nextLineObj.Text.trim()
                    if (nextLineObjTextTrimmed.length <= 0)
                        continue;

                    /** Get length of second part of the hyphenated line break */
                    const secondPartLength = searchTermTrimmed.length - hyphenatedEOLMatch.length;
                    if (secondPartLength <= 0)
                        continue;

                    /* Check if the word at the start of the line is the
                    *  same as the second part of our search term.
                    * */
                    const secondPart = nextLineObjTextTrimmed.substring(0, secondPartLength);
                    if (secondPart == searchTermTrimmed.substring(hyphenatedEOLMatch.length)) {
                        const nextLineMatch = {
                            "ISBN": bookObj.ISBN,
                            "Page": nextLineObj.Page,
                            "Line": nextLineObj.Line
                        };

                        /** Don't push line if it was already matched. This can happen if it was part of a hyphenated line break. */
                        if (!result.Results.some((x) => x.ISBN == match.ISBN && x.Page == match.Page && x.Line == match.Line))
                            result.Results.push(match);
                        
                        result.Results.push(nextLineMatch);
                        /** Although unlikely, we don't skip the next line because it may contain another hyphenated line break match. */
                    }
                }
            }
        }
    }

    return result; 
}

/** Escape string for use with RegExp. */
function escapeRegex(string) {
    return string.replace(/[/\-\\^$*+?.()|[\]{}]/g, "\\$&");
}

/** Verify structure of a line object (bookObj.Content[x]) */
function isLineObjValid(lineObj) {
    if (lineObj == null || typeof(lineObj.Page) != "number" || typeof(lineObj.Line) != "number")
        return false;

    return true;
}

/** Example input object. */
const twentyLeaguesIn = [
    {
        "Title": "Twenty Thousand Leagues Under the Sea",
        "ISBN": "9780000528531",
        "Content": [
            {
                "Page": 31,
                "Line": 8,
                "Text": "now simply went darkness on by her own momentum.  The dark-"
            },
            {
                "Page": 31,
                "Line": 9,
                "Text": "ness was then profound; and however good the Canadian\'s"
            },
            {
                "Page": 31,
                "Line": 10,
                "Text": "eyes were, I asked myself how he had managed to see, and"
            } 
        ] 
    }
]
    
/** Example output object */
const twentyLeaguesOut = {
    "SearchTerm": "the",
    "Results": [
        {
            "ISBN": "9780000528531",
            "Page": 31,
            "Line": 9
        }
    ]
}

/*
 _   _ _   _ ___ _____   _____ _____ ____ _____ ____  
| | | | \ | |_ _|_   _| |_   _| ____/ ___|_   _/ ___| 
| | | |  \| || |  | |     | | |  _| \___ \ | | \___ \ 
| |_| | |\  || |  | |     | | | |___ ___) || |  ___) |
 \___/|_| \_|___| |_|     |_| |_____|____/ |_| |____/ 
                                                      
 */

/* We have provided two unit tests. They're really just `if` statements that 
 * output to the console. We've provided two tests as examples, and 
 * they should pass with a correct implementation of `findSearchTermInBooks`. 
 * 
 * Please add your unit tests below.
 * */

/** We can check that, given a known input, we get a known output. */
const test1result = findSearchTermInBooks("the", twentyLeaguesIn);
if (JSON.stringify(twentyLeaguesOut) === JSON.stringify(test1result)) {
    console.log("PASS: Test 1");
} else {
    console.log("FAIL: Test 1");
    console.log("Expected:", twentyLeaguesOut);
    console.log("Received:", test1result);
}

/** We could choose to check that we get the right number of results. */
const test2result = findSearchTermInBooks("the", twentyLeaguesIn); 
if (test2result.Results.length == 1) {
    console.log("PASS: Test 2");
} else {
    console.log("FAIL: Test 2");
    console.log("Expected:", twentyLeaguesOut.Results.length);
    console.log("Received:", test2result.Results.length);
}

const twentyLeaguesOut3 = {
    "SearchTerm": "darkness",
    "Results": [
        {
            "ISBN": "9780000528531",
            "Page": 31,
            "Line": 8
        },
        {
            "ISBN": "9780000528531",
            "Page": 31,
            "Line": 9
        }
    ]
}

/** Search term with hyphenated line breaks should match next line. */
const test3result = findSearchTermInBooks("darkness", twentyLeaguesIn); 
if (JSON.stringify(twentyLeaguesOut3) === JSON.stringify(test3result)) {
    console.log("PASS: Test 3");
} else {
    console.log("FAIL: Test 3");
    console.log("Expected:", twentyLeaguesOut3);
    console.log("Received:", test3result);
}

const plightIn = [
    {
        "Title": "The Plight of a Unit Test",
        "ISBN": "0000000000000",
        "Content": [
            {
                "Page": 1,
                "Line": 1,
                "Text": "It's 4'o'-"
            },
            {
                "Page": 1,
                "Line": 2,
                "Text": "clock and I use auto-corre-"
            },
            {
                "Page": 1,
                "Line": 3,
                "Text": "ct. Why do my words keep getting split up? Watch: auto--"
            },
            {
                "Page": 1,
                "Line": 4,
                "Text": "correct. Life is unfair as a unit test. auto-"
            },
            {
                "Page": 1,
                "Line": 5,
                "Text": "-correct. Destined to never speak in full sentenc-"
            },
            {
                "Page": 1,
                "Line": 6,
                "Text": "Testin... Testin... Testin-"
            }
        ] 
    }
]

const plightOut1 = {
    "SearchTerm": "4'o'clock",
    "Results": [
        {
            "ISBN": "0000000000000",
            "Page": 1,
            "Line": 1
        },
        {
            "ISBN": "0000000000000",
            "Page": 1,
            "Line": 2
        }
    ]
}

/** A compound word with apostrophes as search term and hyphenated line breaks should match next line. */
const test4result = findSearchTermInBooks("4'o'clock", plightIn); 
if (JSON.stringify(plightOut1) === JSON.stringify(test4result)) {
    console.log("PASS: Test 4");
} else {
    console.log("FAIL: Test 4");
    console.log("Expected:", plightOut1);
    console.log("Received:", test4result);
}

const plightOut2 = {
    "SearchTerm": "auto-correct",
    "Results": [
        {
            "ISBN": "0000000000000",
            "Page": 1,
            "Line": 2
        },
        {
            "ISBN": "0000000000000",
            "Page": 1,
            "Line": 3
        },
        {
            "ISBN": "0000000000000",
            "Page": 1,
            "Line": 4
        },
        {
            "ISBN": "0000000000000",
            "Page": 1,
            "Line": 5
        }
    ]
}

/** A compound word with hyphens as search term and hyphenated line breaks should match next line. */
const test5result = findSearchTermInBooks("auto-correct", plightIn); 
if (JSON.stringify(plightOut2) === JSON.stringify(test5result)) {
    console.log("PASS: Test 5");
} else {
    console.log("FAIL: Test 5");
    console.log("Expected:", plightOut2);
    console.log("Received:", test5result);
}

/** Search term should not match hyphenated line break words. */
const test6result = findSearchTermInBooks("sentenc", plightIn); 
if (test6result.Results.length == 0) {
    console.log("PASS: Test 6");
} else {
    console.log("FAIL: Test 6");
    console.log("Expected:", 0);
    console.log("Received:", test6result.Results.length);
}

const plightOut4 = {
    "SearchTerm": "Testin",
    "Results": [
        {
            "ISBN": "0000000000000",
            "Page": 1,
            "Line": 6
        }
    ]
}

/* Search term should match even if hyphenated line break is same word
 * as long as there are earlier occurrences of the search term in the same line
 * */
const test7result = findSearchTermInBooks("Testin", plightIn); 
if (JSON.stringify(plightOut4) === JSON.stringify(test7result)) {
    console.log("PASS: Test 7");
} else {
    console.log("FAIL: Test 7");
    console.log("Expected:", plightOut4);
    console.log("Received:", test7result);
}